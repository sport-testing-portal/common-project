# common-project

Documentation, Concepts, and Code Snippets that affect multiple projects under the sport testing portal umbrella

Enable a collection of Yii2 extensions to simplify rapid development, code construction, testing, and site expansion.

CEC v0.1.0 - Yii2 Extensions
dektrium/yii2-user
dektrium/yii2-rbac

CEC v0.2.0
yii2-nested-rest
mootensai/yii2-enhanced-gii
yii2-curl
yii2-encrypter
yii2-gravatar
yii2-save-relations-behavior


CEC v0.3.0
yii2-migration-generator (has install issues)


yii2-upload-file

yii2-audit