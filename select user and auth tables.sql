-- flush privileges
select * from yii2basic_utf8.migration;
select * from yii2basic_utf8.profile;
select * from yii2basic_utf8.social_account;
select * from yii2basic_utf8.token;
select * from yii2basic_utf8.user;

select * from yii2basic_utf8.auth_assignment;
select * from yii2basic_utf8.auth_item;
select * from yii2basic_utf8.auth_item_child;
select * from yii2basic_utf8.auth_rule;

-- example from v3 schema to illustrate fields missing in the newer implementation eg dektrium/yii2-user
SELECT * FROM gsm_dev_v3.user_login_attempt;

/*
truncate table token;
truncate table user;
truncate table profile;

delete from user where id = 2;

CREATE SCHEMA `yii2basic_utf8` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
*/

CREATE SCHEMA `yii2usario_utf8` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;