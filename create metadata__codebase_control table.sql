CREATE TABLE `metadata__codebase_control` (
  `codebase_control_id` int(11) NOT NULL AUTO_INCREMENT,
  `codebase_id` int(11) NOT NULL,
  `controller_actions` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action_function` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action_params` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action_url` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `lock` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`codebase_control_id`),
  KEY `fk_metadata__codebase_control_idx` (`codebase_id`),
  CONSTRAINT `fk_metadata__codebase_control` FOREIGN KEY (`codebase_id`) REFERENCES `metadata__codebase` (`codebase_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
